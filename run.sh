#!/bin/bash

# load executables paths
source ./run.rc

#set -x

die() {
    echo $1
    echo "use $0 -h for help"
    exit 2
}

print_help() {
    echo "usage: $0 [OPTIONS] [TESTFILES]"
    echo
    echo "  -h  print this help"
    echo "  -g  run tests only on GCC"
    echo "  -l  run tests only on LLVM"
    echo "  -s  strict, script exits when first test fails"
    echo "  -v  verbose, print error message if test fails"
    echo "  -o  comma separated optimization levels (0,1,2,3 is default)"
    echo ""
    echo "  you can use regex to run some group of tests"
    echo "  example: $0 suite/2*"
    echo ""
    exit 0
}

TESTS=
TARGET=
VERBOSE=
STRICT=
LEVELS="0 1 2 3"

target_flags=0
while getopts ":lghsvo:" flag;
do
    case $flag in
        'l')
            target_flags=$((target_flags | 1))
            ;;
        'g')
            target_flags=$((target_flags | 2))
            ;;
        'h')
            print_help
            ;;
        'v')
            VERBOSE=1
            ;;
        's')
            STRICT=1
            ;;
        'o')
            OFS=$IFS; IFS=','; LEVELS=
            for i in $OPTARG; do
                if [[ ! $i =~ ^(0|1|2|3)$ ]]; then
                    die "$i is not a valid optimization level"
                else
                    LEVELS="${LEVELS} $i"
                fi
            done; IFS=$OFS
            ;;
        '?')
            die "invalid option -$OPTARG -h for help"
            ;;
    esac
done

CHECK="TMI"

if [[ $target_flags -eq 0 ]];then
    TARGET="__llvm __gcc"
    CHECK="$CHECK CLANG LLC CC1"
fi
if [[ $((target_flags & 1)) -eq 1 ]]; then
    TARGET="__llvm"
    CHECK="$CHECK CLANG LLC"
fi
if [[ $((target_flags & 2)) -eq 2 ]]; then
    TARGET="$TARGET __gcc"
    CHECK="$CHECK CC1"
fi

# check that everything is ready to run
for i in $CHECK; do
    if [[ -z ${!i} ]]; then
        echo "$i not defined (see run.rc)"
        exit 2
    fi
    path=`which ${!i}`
    if [[ $? -ne 0 || ! -x $path ]]; then
        echo "${!i} not found (see run.rc)"
        exit 2
    fi
done

arg=$OPTIND
while [[ $arg -le $# ]]; do
    t=${!arg}
    [ ! `file --mime-type -b $t` == 'text/x-c' ] && die "$t is not a C file"
    TESTS="${TESTS} ${!arg}"
    arg=$((arg+1))
done

if [[ -z $TESTS ]];then
    die "no tests to execute"
fi

echo $TARGET
echo $LEVELS

# parse annotation from test file
get_annots() {
    grep '^@.*' $1
}

get_annots_output() {
    echo "$1" | sed -n '/^@OUTPUT/s/^@OUTPUT="\([^"]*\)"/\1/p'
}

get_annots_input() {
    echo "$1" | sed -n '/^@INPUT/s/^@INPUT=//p'
}

get_annots_status() {
    echo "$1" | sed -n '/^@STATUS/s/@STATUS=//p'
}

get_name() {
    name=`basename $1`
    echo ${name%\.*}
}

compile_gcc() {
    opt_level=$1; file=$2; name=$3
    $CC1 $CFLAGS -O${opt_level} -o $TEMP/${name}.s $file 2> "$TEMP/run.sh.compile.out"
    return $?
}

compile_llvm() {
    opt_level=$1; file=$2; name=$3
    $CLANG $CFLAGS -O${opt_level} -S -Wno-incompatible-library-redeclaration -target mips-unknown-linux-gnu -emit-llvm -o $TEMP/${name}.ll $file 2> "$TEMP/run.sh.compile.out"
    [ $? -ne 0 ] && return 1
    $LLC -march=tiny -o $TEMP/${name}.s $TEMP/${name}.ll 2> "$TEMP/run.sh.compile.out"
    return $?
}

run() {
    asm_file=$1
    fail=
    # yep, this is kinda ugly but in this way we can get real output and right status
    # in one shot
    echo -en $INPUT | $TMI $asm_file 2> $TEMP/tmi.stderr; ret=$?;\
echo -n '|'; return $ret
}

check_result() {
    soure_file=$1; run_output=$2; run_status=$3
    fail=0
    annots=`get_annots $soure_file`
    output=`get_annots_output $annots`
    status=`get_annots_status $annots`

    if [[ ! -z "$output" && "$run_output" != `echo -e ${output}|` ]]; then
        echo "result: expected output \"$output\" got \"$run_output\""; echo
        fail=1
    fi
    if [[ ! -z $status && $status -ne $run_status ]]; then
        echo "result: expected status $status got $run_status"; echo
        fail=1
    fi
    return $fail
}

print_status() {
    ret=$?
    if [[ $ret -ne 0 ]]; then
        echo -n "f "
    else
        echo -n "t "
    fi
    return $ret
}

compile_and_run() {
    opt_level=$1; file=$2; compile_fn=$3; compiler_name=$4
    fail_compile=1; fail_run=1; fail_check=1
    name=`get_name $file`
    asm_file=${name}.s
    log="[$compiler_name][O${1}] $file"
    printf "%-70s" "$log"
    $compile_fn $opt_level $file $name
    print_status; fail_compile=$?

    if [[ $fail_compile -eq 0 ]];then
        run_output=`run $TEMP/$name.s`
        run_status=$?
        if [[ ! -s "$TEMP/tmi.stderr" ]];then
            print_status;
            fail_run=$?
            check_msg=`check_result "$file" "$run_output" "$run_status"`
            print_status; fail_check=$?
        else
            echo -ne "f - "
        fi
    else
        echo -ne "- - "
    fi

    if [[ $fail_compile -eq 0 && $fail_run -eq 0 && $fail_check -eq 0 ]]; then
        echo "  OK"
    else
        echo "FAIL"
        [[ ! -z $VERBOSE && $fail_compile -ne 0 ]] && echo && cat "$TEMP/run.sh.compile.out"
        [[ ! -z $VERBOSE && $fail_run -ne 0 ]] && echo && cat "$TEMP/tmi.stderr"
        [[ ! -z $VERBOSE && $fail_check -ne 0 ]] && echo "$check_msg"
        [ ! -z $STRICT ] && exit 1
    fi
}

__gcc() {
    compile_and_run $@ compile_gcc " GCC"
}

__llvm() {
    compile_and_run $@ compile_llvm "LLVM"
}

for c in $TARGET; do
    for o in $LEVELS; do
        for t in $TESTS; do
            annots=`get_annots $t`
            output=`get_annots_output "$annots"`
            exit_code=`get_annots_status "$annots"`
            input=`get_annots_input "$annots"`
            $c $o $t
        done
    done
done

exit 0

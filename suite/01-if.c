/*
@STATUS=7
*/

//volatile int input = 3;
int a = 3;

int main(void) {
    if (a <= 'a') {
        a = 7;
    } else {
        a = 3;
    }
    return a;
}

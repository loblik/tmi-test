/*
@INPUT="foo bar"
@OUTPUT="foo bar"
@STATUS=13
*/

#include "../lib/tiny_std.c"

int main(void) {
    int c;
    while ((c = in()) != EOF)
        out(c);
    return 13;
}

/*
@STATUS=6
*/

int main(void) {

    unsigned char a = 223;
    unsigned char b = 100;

    int ret = 0;

    if ((a + b) == 67) {
        ret |= 0x01;
    } else {
        ret |= 0x02;
    }

    if ((char)(a + b) == 67) {
        ret |= 0x04;
    } else {
        ret |= 0x08;
    }

    return ret;
}

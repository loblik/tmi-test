/*
@STATUS=1
*/

unsigned char add(unsigned char a, unsigned char b) {
    return a + b;
}

int main(void) {

    int a = add(200, 100);

    if (a == 44)
        return 1;
    else
        return 0;
}

/*
@STATUS=255
*/

int add(int a, int b) {
    return a + b;
}

void map(int *res, int *a1, int *a2, int (*fn)(int, int), int size) {

    int i;
    for (i = 0; i < size; i++)
        res[i] = fn(a1[i], a2[i]);
}

int main(void) {

    int a1[] = { 1, 2, 4, 8 };
    int a2[] = { 16, 32, 64, 128};

    map(a1, a1, a2, add, 4);

    int i, sum = 0;
    for (i = 0; i < 4; i++)
        sum += a1[i];

    return sum;
}

/*
@INPUT="asdfgh"
@OUTPUT="qwerty"
@STATUS=2
*/

#include "../lib/tiny_std.c"

int main(void) {

    int input;
    while ((input = in()) != EOF) {
        switch (input) {
            case 'a': out('q'); break;
            case 's': out('w'); break;
            case 'd': out('e'); break;
            case 'f': out('r'); break;
            case 'g': out('t'); break;
            case 'h': out('y'); break;
//            default: out(' ');
        }
    }
    return 2;
}

/*
@STATUS=128
*/

int main(void) {
    int val = 1;
    int i;
    for (i = 0; i < 7; i++)
        val *= 2;

    return val;
}

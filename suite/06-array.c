/*
@STATUS=210
*/

#include "../lib/tiny_std.c"

int main(void) {

    int a[] = { 1,  2,  3,  4,  5,  6,  7,  8,  9, 10,
               11, 12, 13, 14, 15, 16, 17, 18, 19, 20 };

    int i, sum = 0;
    for (i = 0; i < 20; i++)
        sum += a[i];

    return sum;
}

/*
@STATUS=9
*/

#include "../lib/tiny_std.c"

struct cpx {
    int r;
    int i;
};

struct cpx cpx_add(struct cpx a, struct cpx b) {
    struct cpx result;
    result.r = a.r + b.r;
    result.i = a.i + b.i;
    return result;
}



int main(void) {

    struct cpx a = { .r = 1, .i = 2 };
    struct cpx b = { .r = 2, .i = 4 };

    struct cpx r = cpx_add(a, b);

    return r.i + r.r;
}

/*
@OUTPUT="hello world\n"
@STATUS=7
*/

#include "../lib/tiny_std.c"

struct msg {
    char *data;
    int len;
};

void puts(struct msg *m) {
    int i;
    for (i = 0; i < m->len; i++)
        out(*(m->data + i));
}

int main(void) {
    struct msg m;
    m.data = "hello world\n";
    m.len = strlen(m.data);
    puts(&m);
    return 7;
}

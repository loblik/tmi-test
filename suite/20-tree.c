/*
@STATUS=182
*/

#define NULL 0

struct node {
    struct node *child[2];
    int val;
};

struct node *node_init(struct node *l, struct node *r, int val, struct node *new) {
    new->child[0] = l;
    new->child[1] = r;
    new->val = val;
    return new;
}

int tree_contains(struct node *root, int val) {
    if (val == root->val)
        return 1;

    if (val < root->val && root->child[0]) {
        return tree_contains(root->child[0], val);
    }

    if (val > root->val && root->child[1]) {
        return tree_contains(root->child[1], val);
    }

    return 0;
}

int main(void) {
    struct node nodes[5];

    struct node *root = node_init(
                            node_init(NULL, NULL, 13, &nodes[0]),
                            node_init(
                                node_init(NULL, NULL, 20, &nodes[1]),
                                node_init(NULL, NULL, 33, &nodes[2]),
                            21,
                            &nodes[3]),
                        18,
                        &nodes[4]);

    return tree_contains(root, 8) |
           (tree_contains(root, 20) << 1) |
           (tree_contains(root, 18) << 2) |
           (tree_contains(root, 99) << 3) |
           (tree_contains(root, 13) << 4) |
           (tree_contains(root, 21) << 5) |
           (tree_contains(root, -2) << 6) |
           (tree_contains(root, 33) << 7);
}
